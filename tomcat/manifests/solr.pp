define tomcat::solr (
  $solr_link            = 'http://archive.apache.org/dist/lucene/solr/4.9.1/solr-4.9.1.tgz',
  $tomcat_app_dir       = '/usr/share/tomcat',
  $commons_loggins_link = 'http://ftp.unicamp.br/pub/apache//commons/logging/binaries/commons-logging-1.2-bin.tar.gz',
  $slf4j_link           = 'http://www.slf4j.org/dist/slf4j-1.7.12.tar.gz',
  $tmp_dir              = '/tmp',
  $solr_name            = $name,
  $solr_home            = undef, 
  $solr_template        = 'tomcat/tomcat/web.xml.erb'
  ){

  include tomcat
############################################################
#DOWNLOADING PACKAGES
############################################################
  exec {"downloading commons loggins":
    cwd => "${tmp_dir}",
    command => "wget ${commons_loggins_link}",
    unless  => "test -f commons-logging-1.2-bin.tar.gz",
    path    => "/usr/local/bin/:/bin/",
  }

  exec {"downloading slf4j":
    cwd => "${tmp_dir}",
    command => "wget ${slf4j_link}",
    unless  => "test -f slf4j-1.7.12.tar.gz",
    path    => "/usr/local/bin/:/bin/",
  }

  exec {"downloading solr":
    cwd => "${tmp_dir}",
    command => "wget ${solr_link}",
    unless  => "test -f solr-4.9.1.tgz",
    path    => "/usr/local/bin/:/bin/",
  }
############################################################
#EXTRATING PACKAGES
############################################################
  exec {"Extracting commons loggins":
    cwd => "${tmp_dir}",
    creates => "${tmp_dir}/commons-logging-1.2",
    command => "tar -zxf commons-logging-1.2-bin.tar.gz",
    unless  => "test -d ${tmp_dir}/commons-logging-1.2",
    require => Exec["downloading commons loggins"],
    path    => "/usr/local/bin/:/bin/",
  }

  exec {"Extracting slf4j":
    cwd => "${tmp_dir}",
    creates => "${tmp_dir}/slf4j-1.7.12",
    command => "tar -zxf slf4j-1.7.12.tar.gz",
    unless  => "test -d ${tmp_dir}/slf4j-1.7.12",
    require => Exec["downloading slf4j"],
    path    => "/usr/local/bin/:/bin/",
  }

  exec {"Extracting solr":
    cwd => "${tmp_dir}",
    creates => "${tmp_dir}/solr-4.9.1",
    command => "tar -zxf solr-4.9.1.tgz",
    unless  => "test -d ${tmp_dir}/solr-4.9.1",
    require => Exec["downloading solr"],
    path    => "/usr/local/bin/:/bin/",
  }
############################################################
#COPYING FILES
############################################################
  exec {"Copying Commons Loggins jar":
    cwd => "${tmp_dir}/commons-logging-1.2",
    command => "cp commons-logging-*.jar ${tomcat_app_dir}/lib",
    unless  => "test -f ${tomcat_app_dir}/lib/commons-logging-1.2.jar",
    require => Exec["Extracting commons loggins"],
    path    => "/usr/local/bin/:/bin/",
  }

  exec {"Copying slf4j jar":
    cwd => "${tmp_dir}/slf4j-1.7.12",
    command => "rsync -Crap slf4j-*.jar --exclude=slf4j-android-*.jar ${tomcat_app_dir}/lib",
    unless  => "test -f ${tomcat_app_dir}/lib/slf4j-jcl-1.7.12.jar",
    require => Exec["Extracting slf4j"],
    path    => "/usr/local/bin/:/bin/",
  }

  exec {"Copying solr war":
    cwd => "${tmp_dir}/solr-4.9.1",
    command => "cp dist/solr-4.9.1.war ${tomcat_app_dir}/webapps/solr.war",
    unless  => "test -f ${tomcat_app_dir}/webapps/solr.war",
    require => Exec["Extracting solr"],
    notify  => Service[$::tomcat::service_name],
    path    => "/usr/local/bin/:/bin/",
  }

  exec {"Copying solr index":
    cwd => "${tmp_dir}/solr-4.9.1",
    command => "cp -a example/solr/* ${solr_home} | chown -R tomcat.tomcat ${solr_home}",
    unless  => "test -d /etc/tomcat/solr/bin",
    require => File["${solr_home}"],
    notify  => Service[$::tomcat::service_name],
    path    => "/usr/local/bin/:/bin/",
  } 

  exec {"Changing permissions solr index":
    cwd => "${solr_home}",
    command => "chown -R tomcat.tomcat ${solr_home}",
    require => [Exec["Copying solr index"], File["${solr_home}"]],
    path    => "/usr/local/bin/:/bin/",
  }

  file {"${solr_home}":
    ensure  => directory,
    mode    => 0755,
    owner   => "tomcat",
    group   => "tomcat",
    require => Exec["Extracting solr"],
  }

  file {"${tomcat_app_dir}/webapps/solr/WEB-INF/web.xml":
    content => template("${solr_template}"),
    mode    => 0644,
    owner   => "tomcat",
    group   => "tomcat",
    require => Exec["Copying solr war"],
    notify  => Service[$::tomcat::service_name],
  }
}
