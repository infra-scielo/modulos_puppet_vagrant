define wordpress::nginx(
  $vhost_name = $name,
  $server_name,
  $root,
  $cache = false,
) {
  package { "nginx": }
  service { "nginx":
    ensure     => "running",
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => Package["nginx"]
  }
  file { "/etc/nginx/conf.d/${vhost_name}.conf":
    content => template("wordpress/vhost_sample.erb"),
    require => Package["nginx"],
    notify  => Service["nginx"]
  }
}
