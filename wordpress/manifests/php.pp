class wordpress::php {
  package { "php-fpm": }
  service { "php-fpm":
    ensure     => "running",
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => Package["php-fpm"]
  }
  file { "/etc/php.ini":
    content => template("wordpress/php.ini.erb"),
    require => Package["php-fpm"],
    notify  => Service["php-fpm"]
  }
}
