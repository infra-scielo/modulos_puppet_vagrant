class postgresql::server (
  $server_package = $postgresql::params::server_package,
  $locale = $postgresql::params::locale,
  $listen = $postgresql::params::listen_address,
  $port = $postgresql::params::port,
  $ssl = $postgresql::params::ssl,
  $ssl_ca_file   = $postgresql::params::ssl_ca_file,
  $ssl_cert_file = $postgresql::params::ssl_cert_file,
  $ssl_crl_file  = $postgresql::params::ssl_crl_file,
  $ssl_key_file  = $postgresql::params::ssl_key_file,
  $preacl = [],
  $acl = [],
) inherits postgresql::params {

  package { "postgresql-server":
    ensure  => present,
  }

  exec {"postgresql-setup initdb":
    command => "postgresql-setup initdb",
    unless  => "test -d /var/lib/pgsql/data",
    require => Package["postgresql-server"],
    path    => "/usr/local/bin/:/bin/:/sbin/:/usr/bin",
  }

  service { "postgresql":
    ensure => running,
    require => Exec["postgresql-setup initdb"],
  }

  file { "postgresql-server-config":
    name    => "/var/lib/pgsql/data/postgresql.conf",
    ensure  => present,
    content => template('postgresql/postgresql.conf.erb'),
    owner   => 'postgres',
    group   => 'postgres',
    mode    => '0644',
    require => [Package["postgresql-server"],Exec["postgresql-setup initdb"]],
    notify  => Service["postgresql"],
  }

  file { "postgresql-server-hba-config":
    name    => "/var/lib/pgsql/data/pg_hba.conf",
    ensure  => present,
    content => template('postgresql/pg_hba.conf.erb'),
    owner   => 'postgres',
    group   => 'postgres',
    mode    => '0640',
    require => [Package["postgresql-server"],Exec["postgresql-setup initdb"]],
    notify  => Service["postgresql"],
  }

}
