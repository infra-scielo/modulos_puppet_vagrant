class postgresql::params {
  $locale        = 'en_US.UTF-8'

  $ssl           = false
  $ssl_ca_file   = undef  # the default is 'root.crt'
  $ssl_cert_file = undef  # the default is 'server.crt'
  $ssl_crl_file  = undef  # the default is 'root.crl'
  $ssl_key_file  = undef  # the default is 'server.key'

  case $::operatingsystem {
    /(CentOS|RedHat)/: {
      $client_package = 'postgresql'
      $server_package = 'postgresql-server'
      $listen_address = 'localhost'
      $port = 5432
    }
    default: {
      fail("Unsupported platform: ${::operatingsystem}")
    }
  }
}
