class postgresql (
  $client_package = $postgresql::params::client_package,

) inherits postgresql::params {

  package { "postgresql":
    ensure  => present,
  }

}
